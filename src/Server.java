import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class Server {

    private ServerSocket serverSocket;
    private ArrayList<SocketHandler> clients;

    private Thread clientListeningThread;

    public static void main(String[] args) {
        Server serv = new Server(4242);
        serv.bindClients();
    }

    public Server(int port) {
        clients = new ArrayList<>();
        this.serverSocket = this.createServerSocket(port);
        System.out.println("Server running on port:" + port);
    }

    private ServerSocket createServerSocket(int port) {
        try {
            return new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Can't open serveur on port:" + port);
            e.printStackTrace();
            return null;
        }
    }

    private void bindClients() {
        clientListeningThread = new Thread(() -> {
            System.out.println("Listening for clients...");
            while (true) {
                if (!Thread.currentThread().isInterrupted()) {
                    try {
                        SocketHandler newClient = new SocketHandler(serverSocket.accept());
                        readName(newClient);
                        createClientReader(newClient);
                        clients.add(newClient);
                        System.out.println("Connected client with name:" + newClient.getName());
                    } catch (IOException e) {
                        System.err.println("Can't accept client");
                        e.printStackTrace();
                    }
                    Thread.yield();
                } else {
                    break;
                }
            }
        });
        clientListeningThread.start();
    }

    private boolean readName(SocketHandler socketHandler) {
        String name = socketHandler.readUTFSocket();
        if (name == null) {
            System.err.println("No name");
            return false;
        } else {
            socketHandler.setName(name);
            return true;
        }
    }

    private void createClientReader(SocketHandler socketHandler) {
        new Thread(() -> {
            while (true) {
                if (socketHandler.isSocketAlive()) {
                    String readString = socketHandler.readUTFSocket();
                    if (readString != null) {
                        broadcastData(socketHandler, socketHandler.getName() + "/" + readString);
                    } else {
                        clients.remove(socketHandler);
                        System.out.println("Socket cleared, client connected:" + clients.size());
                        break;
                    }
                } else {
                    clients.remove(socketHandler);
                    System.out.println("Socket cleared, client connected:" + clients.size());
                    break;
                }
                Thread.yield();
            }
        }).start();
    }

    private void broadcastData(SocketHandler socket, String data) {
        for (SocketHandler client : clients) {
            if (client != socket) {
                new Thread(() -> {
                    client.writeUTFSocket(data);
                }).start();
            }
        }
    }
}
