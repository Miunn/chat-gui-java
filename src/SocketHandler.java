import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SocketHandler {

    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private String name;

    public SocketHandler(Socket socket) {
        this.socket = socket;
        try {
            this.dis = new DataInputStream(this.socket.getInputStream());
            this.dos = new DataOutputStream(this.socket.getOutputStream());
        } catch (IOException e) {
            System.err.println("Can't get I/O Streams");
            e.printStackTrace();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String readUTFSocket() {
        try {
            return this.dis.readUTF();
        } catch (IOException e) {
            System.err.println("Socket on port:" + socket.getPort() + " has disconnected");
            this.closeSocket();
            return null;
        }
    }

    public int writeUTFSocket(String data) {
        try {
            this.dos.writeUTF(data);
            return 1;
        } catch (IOException e) {
            System.err.println("Socket on port:" + socket.getPort() + " has disconnected");
            this.closeSocket();
            return -1;
        }
    }

    public boolean isSocketAlive() {
        return socket.isBound();
    }

    public void closeSocket() {
        try {
            socket.close();
        } catch (IOException e) {
            System.err.println("Couldn't close socket");
            e.printStackTrace();
        }
    }
}
