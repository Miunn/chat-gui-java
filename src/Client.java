import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private SocketHandler clientSocket;
    private Thread readingThread;
    private Thread writingThread;

    private Scanner stdinScan;
    private Thread readKeyboardThread;

    public static void main(String[] args) {
        Client client = new Client();
        client.runClient();
    }

    public Client() {
        stdinScan = new Scanner(System.in);
        System.out.print("Nom:");
        String name = stdinScan.nextLine();

        this.clientSocket = establishConnection(name);
        this.clientSocket.setName(name);
        this.clientSocket.writeUTFSocket(name);
    }

    private void runClient() {
        this.serverReader();

        readKeyboardThread = new Thread(() -> {
            while (true) {
                if (!Thread.currentThread().isInterrupted()) {
                    if (clientSocket.isSocketAlive()) {
                        String read = readKeyboard();
                        serverWriter(read);
                    } else {
                        System.err.println("Can't write because socket is close, closing client");
                        closeClient();
                    }
                } else {
                    break;
                }
            }
        });

        readKeyboardThread.start();
    }

    private SocketHandler establishConnection(String name) {
        try {
            return new SocketHandler(new Socket("localhost", 4242));
        } catch (ConnectException e) {
            System.out.println("No server running");
            return null;
        } catch (IOException e) {
            System.err.println("Can't connect to server");
            e.printStackTrace();
            return null;
        }
    }

    private void closeClient() {
        if (readingThread != null && readingThread.isAlive()) {
            readingThread.interrupt();
        }

        if (writingThread != null && writingThread.isAlive()) {
            writingThread.interrupt();
        }

        if (clientSocket.isSocketAlive()) {
            clientSocket.closeSocket();
        }

        if (readKeyboardThread != null && readKeyboardThread.isAlive()) {
            readKeyboardThread.interrupt();
        }

        if (stdinScan != null) {
            stdinScan.close();
        }
    }

    private String readKeyboard() {
        System.out.print(">");
        return stdinScan.nextLine();
    }

    private void serverReader() {
        readingThread = new Thread(() -> {
            while (true) {
                if (!Thread.currentThread().isInterrupted()) {
                    String serverData = clientSocket.readUTFSocket();

                    if (serverData == null) {
                        System.out.println("Server closed, closing Client");
                        closeClient();
                        break;
                    } else {
                        displayMsg(serverData.split("/")[0], serverData.split("/")[1]);
                    }
                    Thread.yield();
                } else {
                    break;
                }
            }
        });
        readingThread.start();
    }

    private void serverWriter(String data) {
        writingThread = new Thread(() -> {
            if (!Thread.currentThread().isInterrupted()) {
                if (clientSocket.writeUTFSocket(data) == -1) {
                    System.err.println("Write on socket failed, closing connection");
                    closeClient();
                }
            }
        });
        writingThread.start();
    }

    private void displayMsg(String source, String data) {
        System.out.println("[" + source + "] -> " + data);
        System.out.print(">");
    }
}
